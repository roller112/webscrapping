const puppeteer = require('puppeteer');
const moment = require('moment');
const parseString = require('xml2js').parseString;
const readline = require('readline');

const r1 = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const scrappingURL = 'https://www.msc.com/track-a-shipment?agencyPath=usa';

// let searchKeyword = 'MEDUMM033343';
let searchKeyword = '';

let billOfLading;
let allContainerData = [];

let scrape = async () => {
  const browser = await puppeteer.launch({
    ignoreHTTPSErrors: true,
    ignoreHTTPSErrors: true,
    headless: true,
    args: [
      '--disable-gpu',
      '--disable-software-rasterizer',
      '--disable-setuid-sandbox',
      '--no-sandbox',
      '--disable-extensions',
      '--remote-debugging-port=9222',
      '--disable-translate'
    ]
  });

  console.log('[---Open Browser---]');
  const page = await browser.newPage();
  await page.setViewport({ width: 1500, height: 3000 });

  console.log('[---Open new Page---]');
  console.log('[---Enter URL---]');
  await page.goto(scrappingURL);
  
  console.log('[---Wait 2 seconds---]');
  await page.waitFor(2000);

  // Scrape

  await page.screenshot({path: 'nothanks.png'});
  console.log('[---Screenshot Taken---]');


  // Click No, thanks button
  let searchButton;
  const links = await page.$$('a')
  for (let i=0; i < links.length; i++) {
    let valueHandle = await links[i].getProperty('innerText');
    let idHandle = await links[i].getProperty('id');
    let linkText = await valueHandle.jsonValue();
    let idText = await idHandle.jsonValue();
    if (getText(idText).includes('Main_TrackSearch_hlkSearch')) {
      searchButton = links[i];
    }
    const text = getText(linkText);
    if (text === 'NO, THANKS') {
      console.log('[---Click No Thanks button---]');
      await links[i].click();
      i = links.length;
    } else if (i === links.length - 1) {
      console.log('[---No thanks button not exists---]')  
    }
  }


  // Search input
  const searchInputs = await page.$$('input')
  for (let i=0; i < searchInputs.length; i++) {
    let valueHandle = await searchInputs[i].getProperty('id');
    let inputID = await valueHandle.jsonValue();
    const text = getText(inputID);
    if (text.includes('Main_TrackSearch_txtBolSearch_TextField')) {
      await searchInputs[i].type(searchKeyword);
    }
  }

  // Click search button
  console.log('[---Click search button---]');
  await searchButton.click();
  
  // Wait until the result displayed
  console.log('[---Wait 4 seconds---]');
  await page.waitFor(5000);

  await page.screenshot({path: 'Result.png'});
  console.log('[---Screenshot Taken---]');



  console.log('[---Wait until the table displayed---]');
  try {
    await page.waitForSelector('.resultTable.singleRowTable', {
      timeout: 15000
    });

    await page.screenshot({path: 'Table.png'});
    console.log('[---Screenshot Taken---]');

    // Get bill lading table info
    const billLadingTable = await page.$$eval('.resultTable.singleRowTable tr td', tds => {
      return tds.map((td) => {
        return td.innerHTML;
      })
    })
    billOfLading = getBillLadingData(billLadingTable);
  } catch (error) {
    console.log('[---billTable not found---]');
  }

  // Get all containerToggle
  const containerToggles = await page.$$('.containerToggle')
  let containerIDs = [];
  for (let i=0; i < containerToggles.length; i++) {
    let valueHandle = await containerToggles[i].getProperty('innerText');
    let linkText = await valueHandle.jsonValue();
    const text = getText(linkText);
    containerIDs.push(getContainerIDData(text));
  }

  // Get all resultTable
  const resultTables = await page.$$('.resultTable');
  let containerDatas = [];
  for (let i=1; i < resultTables.length; i++) {
    const resultHtml = await page.evaluate(table => table.innerHTML, resultTables[i]);
    containerDatas.push(getContainerData(resultHtml));
  }

  for (let i=0; i < containerIDs.length; i++) {
    allContainerData.push({
      containerID: containerIDs[i],
      containerData: containerDatas[i]
    });
  }

  let output = {};

  if (containerDatas.length > 0) {
    output = {
      bl: searchKeyword,
      vesselName: containerDatas[0].currentVessel,
      voyageNumber: containerDatas[0].currentVoyage,
      eta: containerDatas[0].estimatedTimeOfArrival,
      status: containerDatas[0].status,
      locations: [
        containerDatas[0].original,
        containerDatas[0].currentLocation,
        containerDatas[0].destination
      ]
    }
    console.log('[---output---]', output);
  } else {
    console.log('[---Shipment Not found---]');
  }

  console.log('[---Close browser---]');
  browser.close();
};
  
r1.question('Input tracking number, container number or BOL number:', (input) => {
  console.log('[---Searching for ---]', input);
  searchKeyword = input;

  if (input) {
    console.log('[---Start scrapping----]');
    scrape().then((value) => {
      // console.log(value); // Success!
    });
  }

  r1.close();
})

async function getElementsFrom(page, selector) {
  const ids = await page.evaluate(() => {
    const list = document.querySelectorAll(selector);
    const ids = [];
    for (const element of list) {
      const id = selector + ids.length;
      ids.push(id);
      element.setAttribute('puppeteer', id);
    }
    return ids;
  }, selector);
  const getElements = [];
  for (const id of ids) {
    getElements.push(page.$(`${selector}[puppeteer=${id}]`));
  }
  return Promise.all(getElements);
}

function getText(linkText) {
  linkText = linkText.replace(/\r\n|\r/g, '\n');
  linkText = linkText.replace(/\ +/g, ' ');

  //Replace &nbsp; with a space
  let nbspPattern = new RegExp(String.fromCharCode(160), 'g');
  return linkText.replace(nbspPattern, ' ');
}

function getBillLadingData(tempData) {
  const tempBegin = `<span class="responsiveTd">`.length;
  const vessel = tempData[1].substring(tempBegin, tempData[1].length - 7);
  const portOfLoad = tempData[2].substring(tempBegin, tempData[2].length - 7);
  const portOfDischarge = tempData[3].substring(tempBegin, tempData[3].length - 7);
  const billOfLoadingData = {
    vessel,
    portOfLoad,
    portOfDischarge
  };
  return billOfLoadingData;
}

function getContainerIDData(tempData) {
  const tempBegin = `Container: `.length;
  return tempData.substring(tempBegin, tempData.length);
}

function getContainerData(tempData) {
  let convertedString = tempData.replace(/\r?\n|\r/g, '');
  convertedString = convertedString.slice(convertedString.indexOf('</thead>') + 8);
  let resultRows = [];

  let estimatedTimeOfArrival = '';
  let currentLocation = '';
  let destination = '';
  let originalLocation = '';
  let currentVessel = '';
  let currentVoyage = '';
  let currentDate;
  let status = 'IN-DELIVERY';

  parseString(convertedString, (err, result) => {
    let trs = result.tbody.tr;
    
    trs = trs.map(item => {
      let tds = item.td;
      const rowData = {
        location: tds[0].span[0]._ ? tds[0].span[0]._.trim() : '',
        description: tds[1].span[0]._ ? tds[1].span[0]._.trim() : '',
        date: tds[2].span[0]._ ? tds[2].span[0]._.trim() : '',
        vessel: tds[3].span[0]._ ? tds[3].span[0]._.trim() : '',
        voyage: tds[4].span[0]._ ? tds[4].span[0]._.trim() : ''
      }
      resultRows.push(rowData);
    });
  })

  if (resultRows.length === 0) {
    return null;
  }

  destination = resultRows[0].location;
  originalLocation = resultRows[resultRows.length - 1].location;

  for (let i=0; i < resultRows.length; i++) {
    if (resultRows[i].description.toLowerCase() === 'estimated time of arrival') {
      estimatedTimeOfArrival = resultRows[i].date;
    }
    let cDate = moment(resultRows[i].date, 'DD-MM-YYYY');
    const nDate = new Date();
    if (cDate <= nDate) {
      currentLocation = resultRows[i].location;
      currentVessel = resultRows[i].vessel;
      currentVoyage = resultRows[i].voyage;
      currentDate = cDate;
      i = resultRows.length;
    }
  }

  if (currentVessel === '') {
    for (let i=0; i < resultRows.length; i++) {
      currentVessel = resultRows[i].vessel;
      currentVoyage = resultRows[i].voyage;
      if (currentVessel != '') {
        i = resultRows.length;
      }
    } 
  }

  if (estimatedTimeOfArrival === '') {
    estimatedTimeOfArrival = currentDate;
    status = 'COMPLETE';
  }

  estimatedTimeOfArrival = moment(estimatedTimeOfArrival,'DD-MM-YYYY').format(moment.HTML5_FMT.DATETIME_LOCAL_MS);

  return {
    estimatedTimeOfArrival,
    currentLocation: splitLocation(currentLocation, true),
    currentVessel,
    currentVoyage,
    destination: splitLocation(destination),
    original: splitLocation(originalLocation),
    status
  };
}

function splitLocation(location, isCurrent) {
  let newLocation = location;
  if (location) {
    newLocation = location.split(', ');
    if (isCurrent) {
      return {
        isCurrent: true,
        city: newLocation[0],
        state: newLocation[1],
        country: newLocation[2]
      }
    }
    return {
      city: newLocation[0],
      state: newLocation[1],
      country: newLocation[2]
    }
  }
  if (isCurrent) {
    return {
      isCurrent: true,
      location
    }
  }
  return {
    location
  };
}

function xmlToJson(xml) {
	
	// Create the return object
	var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj['@attributes'] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj['@attributes'][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof(obj[nodeName]) == 'undefined') {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == 'undefined') {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
};